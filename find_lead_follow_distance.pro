PRO find_lead_follow_distance,x_pos,y_pos,clus_area,clus_mag,mask,d_l_f,s_l_f,weight_cent,lead_cent,foll_cent

; PROGRAMMER: I.Kontogiannis, RCAAM, Academy of Athens, 06/10/2016
;
; PURPOSE: To calculate the distance between the leading and followin sunspot subgroups, according to
;          Howard 1991, SoPh, 136 251 and Baranyi et al. 2015 and the S_l_f separation parameter according
;          to Korsos & Erdelyi 2015
; INPUTS:  The output from find_umbrae_flux.pro
; 

; find weighted center

if total(x_pos) eq 0. or total(y_pos) eq 0 or total(clus_area) eq 0. then begin
  print,'No sunspots, Returning ...'
  d_l_f=0.
  weight_cent=0.
  lead_cent=0.
  foll_cent=0.
  s_l_f=0.
  goto,enda
endif

if n_elements(x_pos) eq 1 or n_elements(y_pos) eq 1 or n_elements(clus_area) eq 1 then begin
  print,'Only one sunspot, Returning ...'
  d_l_f=0.
  weight_cent=0.
  lead_cent=0.
  foll_cent=0.
  s_l_f=0.
  goto,enda
endif

if n_elements(x_pos) eq 2 or n_elements(y_pos) eq 2 or n_elements(clus_area) eq 2 then begin
  print,'Two sunspots, calculating distance: '
  wx=total(x_pos*clus_area/total(clus_area))
  wy=total(y_pos*clus_area/total(clus_area))
  weight_cent=[wx,wy]
  p_east=where(x_pos lt wx,np_east)
  p_west=where(x_pos gt wx,np_west)
  y_east=y_pos(p_east) & x_east=x_pos(p_east)
  y_west=y_pos(p_west) & x_west=x_pos(p_west)
  
  lead_cent=[x_west,y_west]
  foll_cent=[x_east,y_east]
  d_l_f=sqrt((x_west-x_east)*(x_west-x_east)+(y_west-y_east)*(y_west-y_east))
  area_tot=total(clus_area)
  s_l_f=d_l_f/(2.*sqrt(area_tot/!pi))
  goto,enda
endif

;; Calculations for all other cases:

  wx=total(x_pos*clus_area/total(clus_area))
  wy=total(y_pos*clus_area/total(clus_area))

weight_cent=[wx,wy]

; find weighted position of the WEST - EAST PART:

p_east=where(x_pos lt wx,np_east)
p_west=where(x_pos gt wx,np_west)


y_east=y_pos(p_east) & x_east=x_pos(p_east)
area_east=clus_area(p_east)

y_west=y_pos(p_west) & x_west=x_pos(p_west)
area_west=clus_area(p_west)

x_lead=total(area_west*x_west/total(area_west))
y_lead=total(area_west*y_west/total(area_west))

x_foll=total(area_east*x_east/total(area_east))
y_foll=total(area_east*y_east/total(area_east))

lead_cent=[x_lead,y_lead]
foll_cent=[x_foll,y_foll]

d_l_f=sqrt((x_lead-x_foll)*(x_lead-x_foll)+(y_lead-y_foll)*(y_lead-y_foll))
area_tot=total(clus_area)
s_l_f=d_l_f/(2.*sqrt(area_tot/!pi))

enda:

print,'End of calculations'


END
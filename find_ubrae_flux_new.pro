PRO find_ubrae_flux_new,cont_img,mag_img,x_pos,y_pos,clus_area,clus_mag,mask,grad=grad

  ;; PROGRAMMER: I.Kontogiannis, RCAAM, Academy of Athens, 06/10/2016
  ;
  ; INPUTS:  cont_img            -->  the continuum image
  ;          mag_img             -->  the magnetogram
  ;
  ; OUTPUTS: x_pos,y_pos         --> the positions of the umbrae
  ;          clus_area,clus_mag  --> the area ang magnetic flux densities of umbrae
  ;          mask                --> the mask containing the umbrae
  ;
  ; KEYWORDS: grad               --> set this keyword to perform gradient removal

  sz1=size(cont_img,/dim)
  sz2=size(mag_img,/dim)
  if sz1(0) ne sz2(0) or sz1(1) ne sz2(1) then begin
    print,'Incompatible Image Dimensions, Returning ...'
    x_pos=0.
    y_pos=0.
    clus_area=0.
    clus_mag=0.
    mask=replicate(0,sz1(0),sz1(1))
    goto,enda
  endif

  if keyword_set(grad) then grad_removal,cont_img,cimg,grad_img else cimg=cont_img

; trim the edges by setting to 0
  mag_img[0:4,*]=0. & mag_img[sz1(0)-4:sz1(0)-1,*]=0.
  mag_img[*,0:4]=0. & mag_img[*,sz1(1)-4:sz1(1)-1]=0.

  ;cimg=cont_img

  ;; This part taken from the: function umbsel,cimg,mag_img,uilevel,umlevel,pilevel,pmlevel
  ;; of the DeltaFinder:

  uilevel=[0,0.65] ;Intensity levels to choose umbra
  umlevel=[500,7000] ; Magnetic levels to choose umbra
  pilevel=[0.65,0.9]  ; Intensity levels to choose Penumbra
  pmlevel=[50,7000]

  ;Find the Quiet Sun peak from the data
  binsize=max(cimg>0)/100.

  tmp=max(histogram(cimg,min=2*binsize,bin=binsize),maxpos)

  cqsun=(maxpos+2)*binsize
  ; Normalise Continuum and  umbra selection
  cimgn=cimg/cqsun

  ; UMBRA MASK:

  umb_i=cimgn le uilevel[1]
  umb_m=abs(mag_img) ge umlevel[0] and abs(mag_img) le umlevel[1]

  umb_mask=1.*umb_i*umb_m


  if total(umb_mask) eq 0 then begin
    print,'No umbrae detected, returning...'
    x_pos=0.
    y_pos=0.
    clus_area=0.
    clus_mag=0.
    mask=replicate(0,sz1(0),sz1(1))
    goto,enda
  endif

  ; create clusters of penumbrae+umbrae:
  identify_clusters,umb_mask,sz1(0),sz1(1),iclus,cls


;if cls eq 0 or cls eq 1 then begin
;  print,'No spots or 1 spot detected, returning...'
;  x_pos=0.
;  y_pos=0.
;  clus_area=0.
;  clus_mag=0.
;  mask=replicate(0,sz1(0),sz1(1))
;  goto,enda
;endif

if cls eq 0 then begin
  print,'No spots detected, returning...'
  x_pos=0.
  y_pos=0.
  clus_area=0.
  clus_mag=0.
  mask=replicate(0,sz1(0),sz1(1))
  goto,enda
endif

im2=abs(max(cimg)-cimg)
id1=sz1(0) & id2=sz1(1)
;; calculate areas, intensity weighted centroids and magnetic flux density (G)
x_pos=0.
y_pos=0.
clus_area=0.
clus_mag=0.
for i=1,cls do begin & $
  rt=where(iclus eq i,n_rt) & $

  ;; Calculation of flux-weighted centroid coords (xc,yc) and radius rd
  VECART,rt,xp,yp,id1 & $
  xc=fix(total(im2[rt]*xp)/total(im2[rt])+0.5) & $
  yc=fix(total(im2[rt]*yp)/total(im2[rt])+0.5) & $
  ;
  ; check if xc, yc correspond to a nonzero value of im2 - if not, make sure you
  ; assign them to the closest nonzero value
  if im2(xc,yc) eq 0. then begin & $
  dnz = sqrt( (xp-xc)^2. + (yp-yc)^2.) & $
  dnz_min = min(dnz,rmin) & $
  xc = xp(rmin) & $
  yc = yp(rmin) & $
endif & $
; find mean radius
rd=total(sqrt((xc-xp)^2.+(yc-yp)^2.)*im2[rt])/total(im2[rt]) & $

coords=[xc,yc] & $
rind=long(float(yc)*id1+float(xc)) & $
cent_pos=rind & $
r_pos=rd & $
; cent positions
; area in pixels
x_pos=[x_pos,xc] & $
y_pos=[y_pos,yc] & $
clus_area=[clus_area,n_rt] & $
clus_mag=[clus_mag,average(mag_img[rt])] & $
;    window,1
;    plot_image,iclus ge 1
;    loadct,3
;    plots,xc,yc,psy=1,symsize=3,thick=2,col=160
;    loadct,0
;    pause
endfor
x_pos=x_pos[1:*]
y_pos=y_pos[1:*]
clus_area=clus_area[1:*]
clus_mag=clus_mag[1:*]

conv_factor=1.33e5/1.52e6    ; pixel to MHS
clus_area=conv_factor*clus_area
mask=umb_mask

ss=where(mask ne 0 and mag_img lt 0)
mask[ss]=-1
ss=where(mask ne 0 and mag_img gt 0)
mask[ss]=1
enda:

END
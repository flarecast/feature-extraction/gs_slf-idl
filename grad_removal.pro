PRO grad_removal,im,im_out,grad_img

;; PURPOSE: To remove the (residual or not) gradient due to the limb darkening in
;; the cut-out images of the SHARP data.
;; This simple procedure creates a gradient image using the smoothed trend
;; of the intensities along the first 10 and last 10 rows of the image.
;; Of course, it is based on the assumption that the AR is located roughly at the center
;; of the image so the firs and last rows correspond to quiet photosphere.
;; CALLING SEQUENCE:  grad_Removal,im,im_out,grad_img
;; PROGRAMMER: I.Kontogiannis, Academy of Athens, 15/09/2016


sz=size(im,/dim)

line1=smooth(average(im(*,0:10),2),sz(0)/5,/edge_tr)
line2=smooth(total(im(*,sz(1)-10:*),2),sz(0)/5,/edge_tr)
lineav=line1/2.+line2/2.
lineav=lineav/max(lineav)

grad_img=fltarr(sz(0),sz(1))
for j=0,sz(1)-1 do grad_img(*,j)=lineav(*)
im_out=im/grad_img
END
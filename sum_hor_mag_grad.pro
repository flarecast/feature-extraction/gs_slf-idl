PRO sum_hor_mag_grad,cont_img,mag_img,g_s,slf,d_l_f,weight_cent,lead_cent,foll_cent,fit_coeff=fit_coeff,minsize=minsize

  ;; PROGRAMMER: I.Kontogiannis, RCAAM, Academy of Athens, 22/11/2016
  ;
  ; INPUTS:   
  ; cont_img    -->  the continuum image
  ; mag_img     -->  the magnetogram
  ;
  ; OUTPUTS:  
  ; G_S         --> The sum of the horizontal magnetic gradient G_S according to Korsos et al. 2016
  ; SLF         --> the seperation distance
  ; d_l_f       --> the distance between leading and following
  ; weight_cent --> the weighted centroid
  ; lead_cent   --> weighted centroid of the leading and
  ; foll_Cent   --> weighet centroid of the following
  ;
  ; KEYWORDS: 
  ; fit_coeff   --> the coefficients [coeff1,coeff2] of the fitted relationship f(a)=coeff1*lnA+coeff2
  ;                 the default values have been calculated using the sHMIDD data of the Debrecen Observatory
  ; minsize     --> minimum size of detected umbra. Default equal to 0.5 (in accordance with the Debrecen limit)


  if keyword_set(fit_coeff) then begin
    coeff1=fit_coeff[0]
    coeff2=fit_coeff[1]
  endif else begin
    coeff2=842.160
    coeff1=165.008
  endelse

  if keyword_set(minsize) eq 0 then minsize=0.5
  
   find_ubrae_flux_new,cont_img,mag_img,x_pos,y_pos,clus_area,clus_mag,mask,/grad

  ; SIZE LIMITATION
  ss=where(clus_area ge minsize,iss)
  if iss eq 0 then begin
    print,'No sizeable spots within the image! Returning...'
    g_s=0.
    slf=0.
    d_l_f=0.
    weight_cent=[0.,0.]
    lead_cent=[0.,0.]
    foll_cent=[0.,0.]
    goto,enda
  endif

  clus_area=clus_area[ss]
  clus_mag=clus_mag[ss]
  x_pos=x_pos[ss]
  y_pos=y_pos[ss]

  if total(x_pos) eq 0. or total(y_pos) eq 0. or total(clus_area) eq 0. then begin
    print,'No spots found within the image! Returning...'
    g_s=0.
    slf=0.
    d_l_f=0.
    weight_cent=[0.,0.]
    lead_cent=[0.,0.]
    foll_cent=[0.,0.]
    goto,enda
  endif


find_lead_follow_distance,x_pos,y_pos,clus_area,clus_mag,mask,d_l_f,slf,weight_cent,lead_cent,foll_cent

  
f_clus_area=coeff1*alog(clus_area)+coeff2  ; this is the absolute value but it does not affect calculations

spos=where(clus_mag gt 0,npos) & sneg=where(clus_mag lt 0,nneg)

if npos eq 0 or nneg eq 0 then begin
  print,'Only one polarity'
  g_s=0.
  goto,enda  
endif

g_s=0.
for i=0,n_elements(spos)-1 do begin
  for j=0,n_elements(sneg)-1 do begin
    temp=clus_area[spos[i]]*f_clus_area[spos(i)]+clus_area(sneg(j))*f_clus_area(sneg(j)) ; the '+' = -(-)
    d_ij=sqrt((abs(x_pos[spos[i]]-x_pos[sneg[j]]))^2+(abs(y_pos[spos[i]]-y_pos[sneg[j]]))^2)  
    g_s=g_s+temp/d_ij
  endfor
endfor

enda:


END